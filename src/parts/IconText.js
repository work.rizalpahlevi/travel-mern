import React from 'react'

import Button from 'elements/button'

export default function IconText() {
  return (
    <Button className='btn brand-text-icon'>
        Stay<span className='text-gray-900'>cation.</span>
    </Button>
  )
}
