/* eslint-disable */
import React from "react";

import Button from "elements/button";
import BrandIcon from "parts/IconText";
import { useLocation } from "react-router-dom";

export default function Header(props) {
  const getNavLinkClass = (path) => {
    // this.props.location.pathName == path ? "active" : ""
    // return props.location.pathname == path ? "active" : ""
    const loc = useLocation();
    return loc.pathname == path ? "active" : "";
  };

  return (
    <header className="spacing-sm">
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light">
          <BrandIcon></BrandIcon>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav ml-auto">
              <li className={`nav-item ${getNavLinkClass("/")}`}>
                <Button type="link" className="nav-link" href="/">
                  Home
                </Button>
              </li>
              <li className={`nav-item ${getNavLinkClass("/")}`}>
                <Button type="link" className="nav-link" href="/browse-by">
                  Browse By
                </Button>
              </li>
              <li className={`nav-item ${getNavLinkClass("/")}`}>
                <Button type="link" className="nav-link" href="/stories">
                  Stories
                </Button>
              </li>
              <li className={`nav-item ${getNavLinkClass("/")}`}>
                <Button type="link" className="nav-link" href="/agents">
                  Agent
                </Button>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
  );
}
