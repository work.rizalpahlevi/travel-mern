import React from "react";
import { render } from "@testing-library/react";
import Button from "./index";
import { BrowserRouter as Router } from 'react-router-dom'




test("Test Button Disabled", () => {
  const { container } = render(<Button isDisabled></Button>);

  expect(container.querySelector('span.disabled')).toBeInTheDocument()
});

test("render loading text", () => {
  const { container, getByText} = render(<Button isLoading></Button>);

//   to check has loading
  expect(getByText(/loading/i)).toBeInTheDocument()
//   to check has span
  expect(container.querySelector('span')).toBeInTheDocument()
});

test("check btn link external", () => {
  const { container, getByText} = render(<Button type="link" isExternal href=""></Button>);

  expect(container.querySelector('a')).toBeInTheDocument()
});

test("check btn link internal", () => {
  const { container, getByText} = render( <Router><Button type="link" href=""></Button></Router>);

  expect(container.querySelector('a')).toBeInTheDocument()
});
